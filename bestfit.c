#include <stdlib.h>
#include <assert.h>
#include "quaternion.h"
#include "bestfit.h"

/*
  best fit module, ver. 3
  RMSD minimization
  Based on reviews of:
    Horn B. K. P. "Closed-form solution of 
      absolute orientation using unit quaternions." 
      Opt. Soc. Am. A, 4(4), 629 (1987).
*/

extern void dgemm_(char *transa, char *transb, 
		   int *m, int *n, int *k, 
		   double *alpha, 
		   double *a, int *lda, 
		   double *b, int *ldb, 
		   double *beta, 
		   double *c, int *ldc);
extern void dsyevx_(char *jobz, char *range, char *uplo, 
		    int *n, 
		    double *a, int *lda, 
		    double *vl, double *vu, 
		    int *il, int *iu, 
		    double *abstol, 
		    int *m, double *w, 
		    double *z, int *ldz, 
		    double *work, int *lwork, int *iwork,
		    int *ifail, int *info);


int calc_rot_quaternion(int n, double* refstr, double* targetstr, double* mass, double* retq)
{
  double rtensor[3][3];
  int i, j;
 
  double matmax[4][4];

  double *refstr_mass;
  refstr_mass = malloc(sizeof(double) * 3 * n);
  for(i = 0; i < n; ++i)
    for(j = 0; j < 3; ++j)
      refstr_mass[j+3*i]=refstr[j+3*i]*mass[i];

  {
    int C3 = 3;
    double alpha = 1.0;
    double beta = 0.0;
    dgemm_("N", "T", 			// transa, transb
	   &C3, &C3, &n, 		// m, n, k
	   &alpha, 			// alpha
	   targetstr, &C3, 		// a, lda
	   refstr_mass, &C3,		// b, ldb
	   &beta,			// beta
	   &rtensor[0][0], &C3);	// c, ldc
    
  }
  free(refstr_mass);

  // make quaternion calculator matrix (only upper half)
  matmax[0][0] = + rtensor[0][0] + rtensor[1][1] + rtensor[2][2];
  matmax[1][1] = + rtensor[0][0] - rtensor[1][1] - rtensor[2][2];
  matmax[2][2] = - rtensor[0][0] + rtensor[1][1] - rtensor[2][2];
  matmax[3][3] = - rtensor[0][0] - rtensor[1][1] + rtensor[2][2];

  matmax[1][0] = + rtensor[2][1] - rtensor[1][2];
  matmax[2][0] = + rtensor[0][2] - rtensor[2][0];
  matmax[3][0] = + rtensor[1][0] - rtensor[0][1];

  matmax[2][1] = + rtensor[1][0] + rtensor[0][1];
  matmax[3][2] = + rtensor[2][1] + rtensor[1][2];
  matmax[3][1] = + rtensor[0][2] + rtensor[2][0];

  {
    int size = 4;
    int max_eig = 4;
    double dummy = 0.;
    int found;
    double eigval[4];
    double eigvec[1][4];
  

    int lwork = 256;
    double work[lwork];
    int iwork[4*5*2];
    int ifail[4];
    int info;
    dsyevx_("V", "I", "U",
	    &size, &matmax[0][0], &size, // n, a, lda
	    &dummy, &dummy,              // va vb
	    &max_eig, &max_eig,          // il, ih
	    &dummy,                      // abstol
	    &found,                      // m
	    &eigval[0],                  // w
	    &eigvec[0][0],               // z
	    &size,                       // ldz
	    &work[0], &lwork, &iwork[0], // work, lwork, iwork
	    &ifail[0], &info);           // ifail, info

    if (info != 0 ||
	found != 1) {
      assert(0);
      return info;
    }
    for(i = 0; i < 4; ++i){
      retq[i] = eigvec[0][i];
    }
  }
  return 0;
}

static void com_fit_inplace(int n, double *refstr, double *target, double* mass, double* shift)
{
  int i, j;
  double c[3];
  double totmass = 0.;
  // ref: 0-centered, but preserve shift
  shift[0] = shift[1] = shift[2] = 0.;
  for(i = 0; i < n; ++i){
    totmass += mass[i];
    for(j = 0; j < 3; ++j)
      shift[j] += refstr[j + i*3] * mass[i];
  }
  for(i = 0; i < 3; ++i)
    shift[i] /= totmass;
  for(i = 0; i < n; ++i)
    for(j = 0; j < 3; ++j)
      refstr[j + i * 3] -= shift[j];

  // target: 0-centered
  c[0] = c[1] = c[2] = 0.;
  for(i = 0; i < n; ++i)
    for(j = 0; j < 3; ++j)
      c[j] += target[j + i*3] * mass[i];
  for(i = 0; i < 3; ++i)
    c[i] /= totmass;
  for(i = 0; i < n; ++i)
    for(j = 0; j < 3; ++j)
      target[j + i * 3] -= c[j];
}

static void com_fit_reverse(int n, double *target, double *shift)
{
  int i, j;
  for(i = 0; i < n; ++i)
    for(j = 0; j < 3; ++j)
      target[j + i * 3] += shift[j];
}

void fit_inplace_tozero(int n, double* ref_str, double *target_str, double *mass)
{
  int i;
  double shift[3];
  double rotq[4];

  com_fit_inplace(n, ref_str, target_str, mass, shift);
  calc_rot_quaternion(n, ref_str, target_str, mass, rotq);
  for(i = 0; i < n; ++i)
    quaternion_rot_inplace(target_str + i * 3, rotq);
}

void fit_inplace_tozero_(int *n, double* ref_str, double *target_str, double *mass)
{
  fit_inplace_tozero(*n, ref_str, target_str, mass);
}

void fit_vel_inplace_tozero(int n, double *ref_str, double *target_str, double* vel, double *mass)
{
  int i;
  double shift[3];
  double rotq[4];

  com_fit_inplace(n, ref_str, target_str, mass, shift);
  calc_rot_quaternion(n, ref_str, target_str, mass, rotq);
  for(i = 0; i < n; ++i){
    quaternion_rot_inplace(target_str + i * 3, rotq);
    quaternion_rot_inplace(vel        + i * 3, rotq);
  }
}

void fit_vel_inplace_tozero_(int *n, double* ref_str, double *target_str, double *vel, double* mass)
{
  fit_vel_inplace_tozero(*n, ref_str, target_str, vel, mass);
}

void fit_inplace(int n, double* ref_str, double *target_str, double *mass)
{
  int i;
  double shift[3];
  double rotq[4];

  com_fit_inplace(n, ref_str, target_str, mass, shift);
  calc_rot_quaternion(n, ref_str, target_str, mass, rotq);
  for(i = 0; i < n; ++i)
    quaternion_rot_inplace(target_str + i * 3, rotq);
  com_fit_reverse(n, target_str, shift);
  com_fit_reverse(n, ref_str, shift);
}

void fit_inplace_(int *n, double* ref_str, double *target_str, double *mass)
{
  fit_inplace(*n, ref_str, target_str, mass);
}

void fit_vel_inplace(int n, double *ref_str, double *target_str, double* vel, double *mass)
{
  int i;
  double shift[3];
  double rotq[4];

  com_fit_inplace(n, ref_str, target_str, mass, shift);
  calc_rot_quaternion(n, ref_str, target_str, mass, rotq);
  for(i = 0; i < n; ++i){
    quaternion_rot_inplace(target_str + i * 3, rotq);
    quaternion_rot_inplace(vel        + i * 3, rotq);
  }
  com_fit_reverse(n, target_str, shift);
  com_fit_reverse(n, ref_str, shift);
}

void fit_vel_inplace_(int *n, double *ref_str, double *target_str, double* vel, double *mass)
{
  fit_vel_inplace(*n, ref_str, target_str, vel, mass);
}

void fit_vel_force_inplace(int n, double *ref_str, double *target_str, double* vel, double *force, double *mass)
{
  int i;
  double shift[3];
  double rotq[4];

  com_fit_inplace(n, ref_str, target_str, mass, shift);
  calc_rot_quaternion(n, ref_str, target_str, mass, rotq);
  for(i = 0; i < n; ++i){
    quaternion_rot_inplace(target_str + i * 3, rotq);
    quaternion_rot_inplace(vel        + i * 3, rotq);
    quaternion_rot_inplace(force      + i * 3, rotq);
  }
  com_fit_reverse(n, target_str, shift);
  com_fit_reverse(n, ref_str, shift);
}

void fit_vel_force_inplace_(int *n, double *ref_str, double *target_str, double* vel, double *force, double *mass)
{
  fit_vel_force_inplace(*n, ref_str, target_str, vel, force, mass);
}

void fit(int n, double* ref_str, double *target_str, double *ret_str, double *mass)
{
  int i, j;
  double *ref_str_copy;
  ref_str_copy = malloc(sizeof(double) * 3 * n);
  for(i = 0; i < n; ++i)
    for(j = 0; j < 3; ++j)
      ret_str[j + i*3] = target_str[j+i*3];
  for(i = 0; i < n; ++i)
    for(j = 0; j < 3; ++j)
      ref_str_copy[j + i*3] = ref_str[j+i*3];

  fit_inplace(n, ref_str_copy, ret_str, mass);
  free(ref_str_copy);
}
