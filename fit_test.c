#include <stdlib.h>
#include <math.h>
#include <check.h>

#include "bestfit.h"


START_TEST(fit_trivial)
{
  double str[6] = 
    { 1.0, 0.0, 0.0,
      0.0, 1.0, 0.0 } ;
  double ref[6] = 
    { 1.0, 0.0, 0.0,
      0.0, 0.0, 1.0 } ;
  double mass[2] = {1.0, 1.0};
  fit_inplace(2, ref, str, mass);

  fail_unless((fabs(str[0] - 1.0) < 1e-4 &&
	       fabs(str[1] - 0.0) < 1e-4 &&
	       fabs(str[2] - 0.0) < 1e-4 &&
	       fabs(str[3] - 0.0) < 1e-4 &&
	       fabs(str[4] - 0.0) < 1e-4 &&
	       fabs(str[5] - 1.0) < 1e-4),
	      "was: %f %f %f %f %f %f", 
	      str[0], str[1], str[2], str[3], str[4], str[5]);
}
END_TEST

Suite *
quaternion_suite (void)
{
  Suite *s = suite_create ("Fit");
  /* Core test case */
  TCase *tc_core = tcase_create ("Core");
  tcase_add_test (tc_core, fit_trivial);
  suite_add_tcase (s, tc_core);
  
  return s;
}


int main(void)
{
  int number_failed;
  Suite *s = quaternion_suite ();
  SRunner *sr = srunner_create (s);
  srunner_run_all (sr, CK_NORMAL);
  number_failed = srunner_ntests_failed (sr);
  srunner_free (sr);
  return (number_failed == 0) ? EXIT_SUCCESS : EXIT_FAILURE;
}
