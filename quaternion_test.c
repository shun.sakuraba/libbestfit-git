#include <stdlib.h>
#include "quaternion.h"
#include <check.h>

START_TEST(mul_trivial)
{
  double a[4];
  a[0] = 1.;
  a[1] = a[2] = a[3] = 0.; 
  double b[4];
  b[0] = 1.;
  b[1] = b[2] = b[3] = 0.; 

  double c[4];
  quaternion_mul(a, b, c);

  fail_unless(fabs(c[0] - 1.0) < 1e-4,
	      "c[0] == %f, instead of 1.0", c[0]);
  fail_unless(fabs(c[1] - 0.0) < 1e-4,
	      "c[1] == %f, instead of 0.0", c[1]);
  fail_unless(fabs(c[2] - 0.0) < 1e-4,
	      "c[2] == %f, instead of 0.0", c[2]);
  fail_unless(fabs(c[3] - 0.0) < 1e-4,
	      "c[3] == %f, instead of 0.0", c[3]);
}
END_TEST

START_TEST(mul_trivial_ij)
{
  double a[4];
  a[1] = 1.;
  a[0] = a[2] = a[3] = 0.; 
  double b[4];
  b[2] = 1.;
  b[0] = b[1] = b[3] = 0.; 

  double c[4];
  quaternion_mul(a, b, c);

  fail_unless(fabs(c[0] - 0.0) < 1e-4,
	      "c[0] == %f, instead of 0.0", c[0]);
  fail_unless(fabs(c[1] - 0.0) < 1e-4,
	      "c[1] == %f, instead of 0.0", c[1]);
  fail_unless(fabs(c[2] - 0.0) < 1e-4,
	      "c[2] == %f, instead of 0.0", c[2]);
  fail_unless(fabs(c[3] - 1.0) < 1e-4,
	      "c[3] == %f, instead of 1.0", c[3]);
}
END_TEST

START_TEST(mul_trivial_ji)
{
  double a[4];
  a[2] = 1.;
  a[0] = a[1] = a[3] = 0.; 
  double b[4];
  b[1] = 1.;
  b[0] = b[2] = b[3] = 0.; 

  double c[4];
  quaternion_mul(a, b, c);

  fail_unless(fabs(c[0] - 0.0) < 1e-4,
	      "c[0] == %f, instead of 0.0", c[0]);
  fail_unless(fabs(c[1] - 0.0) < 1e-4,
	      "c[1] == %f, instead of 0.0", c[1]);
  fail_unless(fabs(c[2] - 0.0) < 1e-4,
	      "c[2] == %f, instead of 0.0", c[2]);
  fail_unless(fabs(c[3] - (-1.0)) < 1e-4,
	      "c[3] == %f, instead of -1.0", c[3]);
}
END_TEST

START_TEST(mul_trivial_ik)
{
  double a[4];
  a[1] = 1.;
  a[0] = a[2] = a[3] = 0.; 
  double b[4];
  b[3] = 1.;
  b[0] = b[1] = b[2] = 0.; 

  double c[4];
  quaternion_mul(a, b, c);

  fail_unless(fabs(c[0] - 0.0) < 1e-4,
	      "c[0] == %f, instead of 0.0", c[0]);
  fail_unless(fabs(c[1] - 0.0) < 1e-4,
	      "c[1] == %f, instead of 0.0", c[1]);
  fail_unless(fabs(c[2] - (-1.0)) < 1e-4,
	      "c[2] == %f, instead of -1.0", c[2]);
  fail_unless(fabs(c[3] - 0.0) < 1e-4,
	      "c[3] == %f, instead of 0.0", c[3]);
}
END_TEST

START_TEST(rotate_trivial)
{
  double r[4];
  r[0] = 1.;
  r[1] = r[2] = r[3] = 0.; 
  double t[3];
  t[0] = t[1] = 0.;
  t[2] = 4.; 

  quaternion_rot_inplace(t, r);

  fail_unless(fabs(t[0] - 0.0) < 1e-4,
	      "t[0] == %f, instead of 0.0", t[0]);
  fail_unless(fabs(t[1] - 0.0) < 1e-4,
	      "t[1] == %f, instead of 0.0", t[1]);
  fail_unless(fabs(t[2] - 4.0) < 1e-4,
	      "t[2] == %f, instead of 4.0", t[2]);
}
END_TEST

START_TEST(rotate_trivial_i)
{
  double r[4];
  r[1] = 1.;
  r[0] = r[2] = r[3] = 0.; 
  double t[4];
  t[2] = 4.;
  t[0] = t[1] = 0.;

  quaternion_rot_inplace(t, r);

  fail_unless(fabs(t[0] - 0.0) < 1e-4,
	      "t[0] == %f, instead of 0.0", t[0]);
  fail_unless(fabs(t[1] - 0.0) < 1e-4,
	      "t[1] == %f, instead of 0.0", t[1]);
  fail_unless(fabs(t[2] + 4.0) < 1e-4,
	      "t[2] == %f, instead of -4.0", t[2]);

}
END_TEST

START_TEST(rotate_trivial_j)
{
  double r[4];
  r[1] = 1.;
  r[0] = r[2] = r[3] = 0.;
  double t[3];
  t[2] = 4.;
  t[0] = t[1] = 0.; 

  quaternion_rot_inplace(t, r);

  fail_unless(fabs(t[0] - 0.0) < 1e-4,
	      "t[0] == %f, instead of 0.0", t[0]);
  fail_unless(fabs(t[1] - 0.0) < 1e-4,
	      "t[1] == %f, instead of 0.0", t[1]);
  fail_unless(fabs(t[2] + 4.0) < 1e-4,
	      "t[2] == %f, instead of -4.0", t[2]);
}
END_TEST

Suite *
quaternion_suite (void)
{
  Suite *s = suite_create ("Quaternion");
  /* Core test case */
  TCase *tc_core = tcase_create ("Core");
  tcase_add_test (tc_core, mul_trivial);
  tcase_add_test (tc_core, mul_trivial_ij);
  tcase_add_test (tc_core, mul_trivial_ji);
  tcase_add_test (tc_core, mul_trivial_ik);
  tcase_add_test (tc_core, rotate_trivial);
  tcase_add_test (tc_core, rotate_trivial_i);
  tcase_add_test (tc_core, rotate_trivial_j);
  suite_add_tcase (s, tc_core);
  
  return s;
}


int main(void)
{
  int number_failed;
  Suite *s = quaternion_suite ();
  SRunner *sr = srunner_create (s);
  srunner_run_all (sr, CK_NORMAL);
  number_failed = srunner_ntests_failed (sr);
  srunner_free (sr);
  return (number_failed == 0) ? EXIT_SUCCESS : EXIT_FAILURE;
}
