#include <assert.h>
#include <math.h>

#define restrict 

#ifndef QUATERNION_H_INCLUDED
#define QUATERNION_H_INCLUDED

static inline void quaternion_mul(double *a, double *b, double *res)
{
  res[0] = a[0] * b[0] - a[1] * b[1] - a[2] * b[2] - a[3] * b[3];
  res[1] = a[0] * b[1] + a[1] * b[0] + a[2] * b[3] - a[3] * b[2];
  res[2] = a[0] * b[2] - a[1] * b[3] + a[2] * b[0] + a[3] * b[1];
  res[3] = a[0] * b[3] + a[1] * b[2] - a[2] * b[1] + a[3] * b[0];
}

static inline void quaternion_conj(double * restrict a, double * restrict ret)
{
  ret[0] = a[0];
  ret[1] = - a[1];
  ret[2] = - a[2];
  ret[3] = - a[3];
}

static inline void quaternion_conj_inplace(double * restrict a)
{
  a[1] = - a[1];
  a[2] = - a[2];
  a[3] = - a[3];
}

static inline void quaternion_rot(double * restrict pos, double * restrict q, double * restrict ret)
{
  double ar, br, cr, dr;

  assert(fabs(q[0] * q[0] + q[1] * q[1] + q[2] * q[2] + q[3] * q[3] - 1.0) < 0.0001);
 
  ar = + pos[0] * q[1] + pos[1] * q[2] + pos[2] * q[3];
  br = + pos[0] * q[0] - pos[1] * q[3] + pos[2] * q[2];
  cr = + pos[0] * q[3] + pos[1] * q[0] - pos[2] * q[1];
  dr = - pos[0] * q[2] + pos[1] * q[1] + pos[2] * q[0];

  assert(fabs(q[0] * ar - q[1] * br - q[2] * cr - q[3] * dr) < 0.0001);

  ret[0] = q[0] * br + q[1] * ar + q[2] * dr - q[3] * cr;
  ret[1] = q[0] * cr - q[1] * dr + q[2] * ar + q[3] * br;
  ret[2] = q[0] * dr + q[1] * cr - q[2] * br + q[3] * ar;
}

static inline void quaternion_rot_inplace(double * restrict pos, double * restrict q)
{
  double ar, br, cr, dr;

  assert(fabs(q[0] * q[0] + q[1] * q[1] + q[2] * q[2] + q[3] * q[3] - 1.0) < 0.0001);

  ar = + pos[0] * q[1] + pos[1] * q[2] + pos[2] * q[3];
  br = + pos[0] * q[0] - pos[1] * q[3] + pos[2] * q[2];
  cr = + pos[0] * q[3] + pos[1] * q[0] - pos[2] * q[1];
  dr = - pos[0] * q[2] + pos[1] * q[1] + pos[2] * q[0];

  assert(fabs(q[0] * ar - q[1] * br - q[2] * cr - q[3] * dr) < 0.0001);

  pos[0] = q[0] * br + q[1] * ar + q[2] * dr - q[3] * cr;
  pos[1] = q[0] * cr - q[1] * dr + q[2] * ar + q[3] * br;
  pos[2] = q[0] * dr + q[1] * cr - q[2] * br + q[3] * ar;
}

#endif // QUATERNION_H_INCLUDED
