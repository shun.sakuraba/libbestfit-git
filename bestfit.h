#ifndef BESTFIT_H_INCLUDED
#define BESTFIT_H_INCLUDED

int calc_rot_quaternion(int n, double* refstr, double* targetstr, double* mass, double* retq);
void fit_inplace(int n, double* ref_str, double *target_str, double *mass);
void fit_vel_inplace(int n, double *ref_str, double *target_str, double* vel, double *mass);
void fit_vel_force_inplace(int n, double *ref_str, double *target_str, double* vel, double *force, double *mass);
void fit_inplace_tozero(int n, double* ref_str, double *target_str, double *mass);
void fit_vel_inplace_tozero(int n, double *ref_str, double *target_str, double* vel, double *mass);
void fit(int n, double* ref_str, double *target_str, double *ret_str, double *mass);

#endif

